(ns kimok.mysql-queue
  (:require [clojure.java.jdbc :as sql]
            [clojure.spec.alpha :as s]
            [clojure.core.async :refer [<! <!! timeout go]]
            [clojure.tools.logging :refer [warn error]]
            [dotenv :refer [env app-env]]))

; The challenge objective is to create a FiFo multi-queue with messages stored in a MySQL database, with an implementation that is independent of test data or use case.
; 
; In addition your application should...
; 1. Fill in the logic for all of the stubbed functions modifying the namespace as needed to develop the queue application.
; 2. Submit a SQL file that includes the table structure and any other data needed for your application.
; 3. Add additional functionality as needed so that the namspace can be run from lein.
; 4. While not required, any notes on the development or optimizations you built in to your application would be a plus.
; 
; If you need access to a MySQL database we can provide credentials upon request.
; 
; Your completed files can be submitted as a zip file, GitHub repo, or GitHub gist. 

(def db
  {:dbtype   (env :DB_TYPE)
   :dbname   (env :MYSQL_DATABASE)
   :user     (env :MYSQL_USER) 
   :password (env :MYSQL_PASSWORD)})

(def comma-paren
  #(str "(" (apply str (butlast (interleave % (repeat \,)))) ")"))

(defn wipe [] (sql/execute! db ["truncate messages"]))

(s/def ::message string?)
(s/def ::message_type string?)
(s/def ::hidden boolean?)
(s/def ::message-map (s/keys :req-un [::message ::message_type]))

(defn push
  "Pushes the given messages to the queue.
   Returns a list of booleans indicating whether or not each message
   was successfully added to the queue."
  [messages]
  (->> (for [m messages
             :let [message
                   (if (s/valid? ::message-map m)
                     m {:message m :message_type "text"})]]
         (try (sql/insert! db :messages message)
              (catch Exception e
                (warn "message failed to push: " m))))
       (map some?)
       doall))

(defn peek
  "Returns one or more messages from the queue.
   Messages are still visible after this call is made.
   Optional keyword args:
     message-type - filters for messages of the given type
     limit - returns the given number of messages (default: 1)"
  [& {:keys [message-type limit]}]
  (sql/query db
             [(str "select * from messages"
                   " where true"
                   " and hidden = false"
                   (if message-type (str " and message_type = "
                                         \" message-type \"))
                   " order by id asc"
                   " limit " (or limit 1))]))

(defn pop
  "Returns one or more messages from the queue.
   Messages are hidden for the duration (in sec) specified by the
   required ttl arg, after which they return to the front of the queue.
   Optional keyword args:
     message-type - filters for messages of the given type
     limit - returns the given number of messages (default: 1)"
  ;; technically, messages won't return to the front if new messages
  ;; have been pushed in the meantime. not sure if that's important,
  ;; but the auto-increment id can probably be updated somehow to really
  ;; move them to the front. only alternative I see is to delete and
  ;; push again, which could be problematic. -kimo
  [ttl & {:keys [message-type limit] :as args}]
  (let [messages (->> args seq flatten (apply peek))
        ids (map :id messages)
        show (fn [& [hide?]]
               (if-not (empty? ids)
                 (sql/execute!
                  db
                  [(str "update messages set hidden = "
                        (some? hide?)
                        " where id in "
                        (comma-paren ids))])))
        hide #(show false)]
    (hide)
    (go (<! (timeout (int (* ttl 1000))))
        (show))
    messages))

(defn confirm
  "Deletes the given messages from the queue.
   This function should be called to confirm the successful handling
   of messages returned by the pop function."
  [messages]
  (let [ids (-> #(if (s/valid? ::message-map %) (:id %) %)
                (map messages))
        response 
        (-> (sql/execute!
             db
             [(str "delete from messages where id in "
                   (comma-paren ids))])
            first)]
    (if (not= response (count ids)) (warn "confirm: some ids not deleted"))
    response))

(defn queue-length
  "Returns a count of the number of messages on the queue.
   Optional keyword args:
     message-type - filters for message of the given type
     with-hidden? - if truthy, includes messages that have been
                    popped but not confirmed"
  [& {:keys [message-type with-hidden?]}]
  (-> (sql/query db
                 [(str "select count(*) from messages"
                       " where true"
                       (if message-type
                         (str " and message_type = "
                              \" message-type \"))
                       (if-not with-hidden?
                         (str " and hidden = false")))])
       first first val))
