(ns kimok.mysql-queue-test
  (:require [clojure.test :refer :all]
            [kimok.mysql-queue :refer :all]
            [clojure.java.jdbc :as db]
            [clojure.spec.alpha :as s]
            [clojure.core.async :refer [<! timeout go]]))

(deftest mysql-queue-test
  (wipe)
  (testing "push"
    (is (= '(true false true false true)
           (push ["hello world" nil 10
                  {:message "failed message"
                   :message_type "invalid"}
                  {:message "successful message"
                   :message_type "custom"}]))))
  (testing "peek"
    (is (s/valid?
         (s/coll-of :kimok.mysql-queue/message-map)
         (peek :message-type "text" :limit 2))))
  (let [popped (pop 10 :message-type "text" :limit 2)]
    (testing "pop"
      (is (s/valid?
           (s/coll-of :kimok.mysql-queue/message-map)
           popped)))
    (testing "queue-length"
      (is (= 1 (queue-length)))
      (is (= 3 (queue-length :with-hidden? true))))
    (testing "confirm"
      (is (= 2 (confirm popped)))
      (is (= 1 (queue-length :with-hidden? true))))))
