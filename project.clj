(defproject mysql-queue "0.1.0-SNAPSHOT"
  :description "coding challenge: a FiFo multi-queue with messages stored in a MySQL database, with an implementation that is independent of test data or use case."
  :url "https://gitlab.com/kimok/mysql-queue"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/java.jdbc"0.7.12"]
                 [mysql/mysql-connector-java "8.0.25"]
                 [org.clojure/core.async "1.3.618"]
                 [org.clojure/tools.logging "1.1.0"]
                 [lynxeyes/dotenv "1.1.0"]]
  :repl-options {:init-ns kimok.mysql-queue})
