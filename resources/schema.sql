drop database if exists multiqueue;
create database multiqueue;
use multiqueue;
create table messages(
       id int unsigned auto_increment not null primary key,
       message text not null,
       message_type text,
       hidden boolean default false not null
);
delimiter $$
create trigger	invalid before insert on messages
       for each row
       begin
	if new.message_type = 'invalid' then
       	   signal sqlstate '45000';
       	end if;
       end$$
